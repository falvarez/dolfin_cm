#coding=utf8

################################################################################
###                                                                          ###
### Created by Martin Genet, 2018-2019                                       ###
###                                                                          ###
### École Polytechnique, Palaiseau, France                                   ###
###                                                                          ###
################################################################################

import dolfin

import dolfin_cm as dcm
from Material_Elastic import ElasticMaterial

################################################################################

class DevElasticMaterial(ElasticMaterial):

    pass
